<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $search = $request->input('keyword');
        $orderBy = $request->input('orderBy');
        //Sap xep
        $users = User::orderBy('id','asc');
        if ($orderBy)
        {
            switch ($orderBy){

                case 'DESC-ID':
                    $users = User::orderBy('id','desc');
                    break;
                case 'ASC-ID':
                    $users = User::orderBy('id','asc');
                    break;
                case 'ASC-NAME':
                    $users = User::orderBy('name','asc');
                    break;
                case 'DESC-NAME':
                    $users = User::orderBy('name','desc');
                    break;
            }
        }
        //tim kiem
        if($search)
        {
            $users->where('name','like',"%$search%")
                ->orwhere('id','like',"%$search%");
        }
        $data['users'] = $users->paginate(5);
        $data['keyword'] = $search;
        $data['orderBy'] = $orderBy;
        return view('admin.users.index', $data);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
        ], [
            'name.required' => 'Please enter name', 'email.required' => 'Please enter email',
            'email.unique' => 'Email existed', 'email.email' => 'No must format email',
            'password.required' => 'Please enter password',
        ]);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        } else {
            $users = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
            ]);

            return redirect()->route('users.index')->with('message', "Added $users->name successfully !");
        }
    }

    public function show($id)
    {
        $data['user'] = User::find($id);
        if ($data['user'] !== null) {
            return view('admin.users.edit', $data);
        }
        return redirect()->route('users.index')->with('error', "This user could not be found!");
    }
    public function edit($id)
    {
        $data['user'] = User::find($id);
        return view('admin.users.edit', $data);
    }
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required', 'email' => 'required|email|unique:users,email,'.$id,
        ], [
            'name.required' => 'Please enter name',
            'email.required' => 'Please enter email',
            'email.unique' => 'Email existed', 'email.email' => 'No must format email',
        ]);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        } else {
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if ($request->input('password')) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();

            return redirect()->route('users.index')->with('message', "Updated $user->name successfully !");
        }
    }

    public function destroy(User $user)
    {

        if ($user->id == Auth::user()->id)
        {
            return redirect()->route('users.index')->with('error', "You can not delete yourself !");

        }
        else {
            $user->delete();
            return redirect()->route('users.index')->with('message', "Delete $user->name successfully !");
        }
    }
}

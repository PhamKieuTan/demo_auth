<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $search = $request->input('keyword');
        $orderBy = $request->input('orderBy');

        //$posts = Post::orderBy('id','desc');

        /*dung function with() de giai quyet van de tranh query nhieu lan*/

        $posts = Post::with('user')->orderBy('id','desc');

        $user['users'] = User::orderBy('id','asc')->get();

        ////Xu ly tag selection

        if($orderBy)
        {
            $posts->where('user_id',$orderBy);
        }

        //tim kiem
        if ($search)
        {
            $posts->where(function ($query) use ($search){
              $query->where('title','like',"%$search%")
                    ->orwhere('content','like',"%$search%");
            });
        }
        $data['posts'] = $posts->paginate(5);
        $data['keyword'] = $search;
        $data['orderBy'] = $orderBy;
        return view('admin.posts.index',$data,$user);

    }

    public function create()
    {
        return view('admin.posts.create');
    }

    public function store(Request $request)
    {
        $data = Post::create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'user_id' => auth()->id(),
        ]);

        return redirect()->route('posts.index', $data)->with('message', "Created $data->title successfully !");
    }
    public function show($id)
    {
        $data['posts'] = Post::find($id);
        if ($data['posts'] !== null)
        {
            return view('admin.posts.edit',$data);
        }
        return redirect()->route('posts.index')->with('error','Not found !');
    }
    public function edit($id)
    {
        $data['posts'] = Post::find($id);
        return view('admin.posts.edit', $data);
    }
    public function update(Request $request , $id)
    {
        $data = Post::find($id);
        $data->title = $request->input('title');
        $data->content = $request->input('content');
        $data->save();

        return redirect()->route('posts.index',$data)->with('message','Editted seccessfully !');
    }

    //public function destroy($id)
    //{
    //    $data = Post::find($id);
    //
    //    if ($data !== null)
    //    {
    //        $data->delete($id);
    //        return redirect()->route('posts.index')->with('message', "Deleted successfully !");
    //    }
    //    return redirect()->route('posts.index')->with('error', "Not successfully !");
    //}

    public function delete($id)
    {
        $post = Post::find($id);
        //dd($post->toArray());
        if ($post->user_id == Auth::user()->id )
        {
            $post->delete();
            //return response()->json(compact('post'),'200');
            return response()->json([
                'message' => 'Delete post successfully !'
            ],'200');
        }
        return response()->json([
            'error' => 'You have not access !'
        ],'403');
    }


}

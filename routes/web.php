<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

 ////group users & posts
Route::group(['prefix'=>'admin'],function (){
    Route::resource('users','UserController');
    Route::resource('posts','PostController',['except'=>['destroy']]);
});


 ////use ajax to delete posts
//Route::get('admin/posts/delete/{id}','PostController@delete');
Route::delete('admin/posts/delete/{id}','PostController@delete');

 ////users
//Route::resource('admin/users','UserController',['as'=>'admin','only'=>['index','create','store','show','update','destroy','edit']]);
//Route::resource('admin/users','UserController', ['as'=>'admin','except'=>['show']]);

//// posts
//Route::resource('admin/posts','PostController',['as'=>'admin','only'=>['index','create','store','show','update','destroy','edit']]);
//Route::resource('admin/posts', 'PostController', ['except'=>['update']]);

@extends('admin.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <h3>Create new posts </h3>
                </div>
                <div class="panel panel-default">
                    <br>
                    <div class="panel-body">
                        <form action="{{route('posts.store')}}" method="POST">
                            @csrf
                            <div class="form-group has-success">
                                <label for="title">Title *</label>
                                <input type="text" class="form-control" id="title"  name="title" required="required" placeholder="title" value="{{old('title')}}">
                            </div>
                            <div class="form-group has-success">
                                <label for="content">Content *</label>
                                <textarea name="content" id="content" class="form-control" cols="30" rows="10" maxlength="65525" required="required"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




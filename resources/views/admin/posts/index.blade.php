@extends('admin.master')
@section('content')
    <div class="container">
        <div>
            {{--add new pos--}}
            <a href="{{route('posts.create')}}" class="btn btn-dark"> + Create</a>
            <br>
        </div>

        <br>
        <div class="row">
            <div class="col-md-8">
                <div class="col-xs-8 col-xs-offset-2">
                    <form action="{{route('posts.index')}}" method="GET">
                        @csrf
                        <div class="input-group">
                            <span class="btn btn-outline-secondary">Filter by: </span>
                            <select name="orderBy" id="orderBy">
                                <option value="">Choose by name</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}"  name="key"
                                            {{ $orderBy == $user->id ?'selected':'' }}
                                    >{{$user->name}}</option>
                                @endforeach()
                            </select>
                            <input type="search" class="form-control" id="keyword" name="keyword" placeholder="Search title,content..."
                                   value="{{$keyword}}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">
                                    <span class="fa fa-search"></span>
                                    Search
                                </button>
                            </span>
                        </div>

                    </form>
                    <br>
                </div>
            </div>
            <div class="col-md-12">
                <br>
                <div class="panel panel-default">
                    <div class="" style="font-size: x-large;color: coral;">LIST POSTS</div>
                    <br>
                    <div class="panel-body">
                        @if(session('message'))
                            <div class="alert alert-success" id="message">{{session('message')}}</div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger" id="error">{{session('error')}}</div>
                        @endif
                        <script language="JavaScript">
                            setTimeout(function(){$('#message').remove()},3000)
                        </script>
                        <script language="JavaScript">
                            setTimeout(function(){$('#error').remove()},3000)
                        </script>
                        <table class="table" id="table-data">
                            <thread>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>User_Name</th>
                                    <th>Create_at</th>
                                    <th>Update_at</th>
                                    <th>Options</th>
                                </tr>
                            </thread>
                            <tbody>
                            @forelse($posts as $post)
                                <tr id="row-{{ $post->id }}">
                                    <td>{{$post->id}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->content}}</td>
                                    <td><a href="{{route('users.edit',['id'=>$post->user_id])}}">{{$post->user->name}}</a></td>
                                    <td>{{$post->created_at}}</td>
                                    <td>{{$post->updated_at}}</td>
                                    <td>
                                        <a href="{{route('posts.edit',['id'=>$post->id])}}" class="btn btn-success"><i class="fa fa-edit"></i></a>

                                        <!--/*dung dialog bootstrap*/ -->
{{--                                        @include('admin.posts.destroy')--}}

                                        <!--/*dung window.confirm*/ -->
{{--                                        <a href="{{route('posts.destroy',['id'=>$post->id])}}" class="btn btn-danger"--}}
{{--                                        onclick="event.preventDefault();window.confirm('Are you sure ?')?--}}
{{--                                        document.getElementById('user-delete-{{$post->id}}').submit():0;"><i class="fa fa-trash"></i></a>--}}
{{--                                        <form action="{{route('posts.destroy',['id'=>$post->id])}}" method="POST" id="user-delete-{{$post->id}}">--}}
{{--                                        {{csrf_field()}}--}}
{{--                                        {{method_field('delete')}}--}}
{{--                                        </form>--}}

                                        <!--/*dung ajax de delete*/ -->
{{--                                        <button class="btn btn-danger btn-delete-post" data-id='{{ $post->id }}'  data-token="{{csrf_token()}}"><i class="fa fa-trash"></i></button>--}}
                                        <button class="btn btn-danger btn-delete-post" data-id='{{ $post->id }}' ><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7">No Data!</td>
                                </tr>
                            @endforelse
                            </tbody>

                        </table>
                        <div class="text-center">
                            {{$posts->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
         $(document).ready(function () {
           $(".btn-delete-post") .on('click',function(){
              var id = $(this).data('id');
              // var token = $(this).data('token')
              if(confirm("Are you sure you want to delete this data?")){
                 $.ajax({
                     url:"/admin/posts/delete/" + id,
                     //1// method:"GET",

                     type:"DELETE",
                     //2//type:"POST",
                     // data: {_method: 'delete', _token :token},
                     success:function ()
                     {
                         $("#row-" + id).remove();
                         alert('Deleted post successfully !');
                     },
                     error:function () {
                         alert('You have not access,please choose other !');
                     }
                 })
              }
              else {
                  return false;
              }
           });
         });
    </script>
@endsection
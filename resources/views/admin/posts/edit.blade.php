@extends('admin.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <h3>Edit new posts </h3>
                </div>
                <div class="panel panel-default">
                    <br>
                    <div class="panel-body">
                        <form action="{{route('posts.update',['id' => $posts->id])}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group has-success">
                                <label for="title">Title *</label>
                                <input type="text" class="form-control" id="title"  name="title" placeholder="title" value="{{$posts->title}}">
                            </div>
                            <div class="form-group has-success">
                                <label for="content">Content *</label>
                                <textarea name="content" id="content" class="form-control" cols="30" rows="10">{{$posts->content}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-success">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




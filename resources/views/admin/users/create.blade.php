@extends('admin.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
           <h3>CREATE NEW USER</h3>
        </div>
        <div class="col-md-8">
            <div>
                <a href="{{route('users.index')}}"></a>
            </div>
            <div class="panel panel-default">
                <br>
                <div class="panel-body">
                    <form action="{{route('users.store')}}" method="POST">
                        @csrf
                        <div class="form-group has-success">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name"  name="name" placeholder="Name" value="{{old('name')}}">
                            <span id="helpBlock2" class="help-block">{{$errors->first('name')}}</span>
                        </div>
                        <div class="form-group has-success">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{old('email')}}">
                            <span id="helpBlock2" class="help-block">{{$errors->first('email')}}</span>
                        </div>
                        <div class="form-group has-success">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            <span id="helpBlock2" class="help-block">{{$errors->first('password')}}</span>
                        </div>
                        <div class="form-group has-success">
                            <label for="password_confirmation ">Confirm Password</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



{{--FORM BOOTSTRAP--}}
{{--<button class="btn btn-dark" data-toggle="modal" data-target="#create">+ Create</button>--}}
{{--<div class="modal" tabindex="-1" role="dialog" id="create">--}}
{{--    <div class="modal-dialog" role="document">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-header">--}}
{{--                <h5 class="modal-title"></h5>--}}
{{--                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                    <span aria-hidden="true">&times;</span>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="modal-body">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <br>--}}
{{--                        <div class="panel panel-default">--}}
{{--                            <div class="panel-heading">ADD USER</div>--}}
{{--                            <br>--}}
{{--                            <div class="panel-body">--}}
{{--                                <form action="{{route('admin.users.store')}}" method="POST">--}}
{{--                                    @csrf--}}
{{--                                    <div class="form-group has-success">--}}
{{--                                        <label for="name">Name</label>--}}
{{--                                        <input type="text" class="form-control" id="name"  name="name" placeholder="Name" value="{{old('name')}}">--}}
{{--                                        <span id="helpBlock2" class="help-block">{{$errors->first('name')}}</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group has-success">--}}
{{--                                        <label for="email">Email address</label>--}}
{{--                                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{old('email')}}">--}}
{{--                                        <span id="helpBlock2" class="help-block">{{$errors->first('email')}}</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group has-success">--}}
{{--                                        <label for="password">Password</label>--}}
{{--                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">--}}
{{--                                        <span id="helpBlock2" class="help-block">{{$errors->first('password')}}</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group has-success">--}}
{{--                                        <label for="password_confirmation ">Confirm Password</label>--}}
{{--                                        <input type="password" class="form-control" id="password_confirmation " name="password_confirmation " placeholder="Password">--}}
{{--                                    </div>--}}
{{--                                    <button type="submit" class="btn btn-success">Create</button>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}



@extends('admin.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>EDIT USER </h3>
            </div>
            <div class="col-md-8">
                <br>
                <div class="panel panel-default">
                    <br>
                    <div class="panel-body">
                        <form action="{{route('users.update',['id'=>$user->id])}}" method="POST" id="edit-form">
                            @csrf
                            {{method_field('put')}}
                            <div class="form-group has-success">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name"  name="name" placeholder="Name" value="{{$user->name}}">
                                <span id="helpBlock2" class="help-block">{{$errors->first('name')}}</span>
                            </div>
                            <div class="form-group has-success">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{$user->email}}">
                                <span id="helpBlock2" class="help-block">{{$errors->first('email')}}</span>
                            </div>
                            <div class="form-group has-success">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                <span id="helpBlock2" class="help-block">{{$errors->first('password')}}</span>
                            </div>
                            <div class="form-group has-success">
                                <label for="password_confirmation ">Confirm Password</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-success">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
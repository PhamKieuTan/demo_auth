<button class="btn btn-danger" data-toggle="modal" data-target="#delete-{{$user->id}}">Delete</button>
<div class="modal" tabindex="-1" role="dialog" id="delete-{{$user->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete {{$user->name}}?</p>
            </div>
            <form action="{{route('users.destroy',['id'=>$user->id])}}" method="POST"
                  id="user-delete-{{$user->id}}">
                {{csrf_field()}}
                {{method_field('delete')}}
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger" name="" id="">Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
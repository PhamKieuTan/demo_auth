@extends('admin.master')
@section('content')
    <div class="container">
        <div>
            {{--add new user--}}
{{--            @include('users.create')  --}}
            <a href="{{route('users.create')}}" class="btn btn-dark"> + Create</a>
            <br>
        </div>

        <br>
        <div class="row">
             {{--search--}}
            <div class="col-md-6">
                <div class="col-xs-8 col-xs-offset-2">
                    <form action="{{route('users.index')}}" method="GET">
                        @csrf
                        <div class="input-group">
                            <span class="btn btn-outline-secondary">Filter by: </span>
                            <select name="orderBy">
                                <option value="null" {{ !$orderBy?'selected':'' }}>Choose sort</option>
                                <option value="DESC-ID" {{ $orderBy == 'DESC-ID'?'selected':'' }}>DESC ID</option>
                                <option value="ASC-ID" {{ $orderBy == 'ASC-ID'?'selected':'' }}>ASC ID</option>
                                <option value="ASC-NAME" {{ $orderBy == 'ASC-NAME'?'selected':'' }} >ASC NAME</option>
                                <option value="DESC-NAME" {{ $orderBy == 'DESC-NAME'?'selected':'' }}>DESC NAME</option>
                            </select>
                            <input type="search" class="form-control" id="keyword" name="keyword" placeholder="Search name..."
                                   value="{{$keyword}}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><span class="fa fa-search">
                                </span>Search</button>
                            </span>
                        </div>

                    </form>
                    <br>
                </div>
            </div>

            <div class="col-md-1">
                <a href="{{route('users.index')}}" class="btn btn-dark">List users</a>
            </div>
            <div class="col-md-12">
            {{-- show list users--}}
                <br>
                <div class="panel panel-default">
                    <div class="" style="font-size: x-large;color: coral;">LIST USERS</div>
                    <br>
                    <div class="panel-body">
                        @if(session('message'))
                            <div class="alert alert-success" id="message">{{session('message')}}</div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger" id="error">{{session('error')}}</div>
                        @endif
                        <script language="JavaScript">
                            setTimeout(function(){$('#message').remove()},3000)
                        </script>
                        <script language="JavaScript">
                            setTimeout(function(){$('#error').remove()},3000)
                        </script>
                        <table class="table">
                            <thread>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Create_at</th>
                                    <th>Update_at</th>
                                    <th>Options</th>
                                </tr>
                            </thread>
                            <tbody>
                                @forelse($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->created_at}}</td>
                                        <td>{{$user->updated_at}}</td>
                                        <td>
                                            {{--edit--}}
{{--                                            @include('users.update')--}}
                                            <a href="{{route('users.edit',['id'=>$user->id])}}" class="btn btn-success"
                                               >Edit</a>
                                            {{--delele--}}
                                            @include('admin.users.delete')
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Data!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                         
                        </table>
                        <div class="text-center">
                            {{$users->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection